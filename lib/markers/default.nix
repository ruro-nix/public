{ lib, ... }:
{
  getMarkers = item: if item ? _markers then item._markers else [];
  getObject = item: if item ? _markers then item._object else item;
  markObject = marker: item: let
    allMarkers = (lib.z.getMarkers item) ++ [ marker ];
    _markers = (
      if lib.allUnique allMarkers
      then allMarkers
      else throw "Duplicate markers in ${lib.toString allMarkers}"
    );
    _object = lib.z.getObject item;
    result = { inherit _markers _object; };
  in
    result;
  markObjectOrList = marker: item: (
    if lib.isList item
    then lib.map (lib.z.markObject marker) item
    else lib.z.markObject marker item
  );
  mergeObjectOrList = item: let
    object = lib.z.getObject item;
    markers = lib.z.getMarkers item;
    conditional = lib.all (marker: marker.cond) markers;
    merged = (
      if lib.isList object
      then lib.mkMerge (lib.map lib.z.mergeObjectOrList object)
      else [ object ]
    );
    result = (
      if markers != {}
      then lib.mkIf conditional merged
      else merged
    );
  in
    result;
}
