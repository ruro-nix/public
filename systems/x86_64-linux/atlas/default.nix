{ config, pkgs, lib, ... }: {
  z.device = {
    hasNvidia = true;
    headless = true;
    isMSI = false;
    minimal = false;

    hostId = "010a0a00";
    ethrInterfaces = [ "eno1" ];
    wlanInterfaces = [ "wlp3s0" ];
  };
  system.stateVersion = "24.11";

  # TODO:
  #   - ensure DualBoot works
  #       - https://wiki.archlinux.org/title/GRUB#Installation
  #       - efibootmgr?
  #
  #   - SSH port
  #   - openrgb
  #   - sshtunnel
  #   - gpg keys
  #
  #   - migrate from arch / old ssd
  #       - mount old ssd
  #       - back up old ssd contents to ZFS (in snapshot)
  #       - reminder to revisit and clear old ssd after ~6 months
}
