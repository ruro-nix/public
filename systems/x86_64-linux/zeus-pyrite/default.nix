{ config, pkgs, lib, ... }: {
  z.device = {
    hasNvidia = false;
    headless = false;
    isMSI = false;
    minimal = true;

    hostId = "d1037592";
    ethrInterfaces = [ "enp2s0" ];
    wlanInterfaces = [ "wlp3s0" ];
  };
  i18n.defaultLocale = lib.mkForce "ru_RU.UTF-8";
  services.xserver.xkb.options = lib.mkForce "grp:alt_shift_toggle";
  users.users.zeus = {
    password = "";
    isNormalUser = true;
    extraGroups = [ "audio" "bluetooth" ];
  };
  system.stateVersion = "24.11";
}
