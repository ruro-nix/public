{ config, pkgs, lib, ... }: {
  z.device = {
    hasNvidia = true;
    headless = false;
    isMSI = true;
    minimal = false;

    hostId = "a1f66df2";
    ethrInterfaces = [ "enp47s0" ];
    wlanInterfaces = [ "wlan0" ];
  };
  system.stateVersion = "23.11";
}
