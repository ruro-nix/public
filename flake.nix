{
  inputs = {
    # ==== Dependencies ====
    nixpkgs.url               = "github:nixos/nixpkgs/nixos-unstable";

    # TODO: home-manager.url          = "github:nix-community/home-manager/release-23.05";
    # TODO: nixos-generators.url      = "github:nix-community/nixos-generators";
    # TODO: nixos-hardware.url        = "github:nixos/nixos-hardware";

    snowfall-lib.url          = "github:snowfallorg/lib";
    # TODO: flake-utils.url           = "github:numtide/flake-utils";
    # TODO: flake-utils-plus.url      = "github:gytis-ivaskevicius/flake-utils-plus";

    # ==== Follows ====
    # Note: home-manager.inputs           = { nixpkgs.follows = "nixpkgs"; };
    # Note: nixos-generators.inputs       = { nixpkgs.follows = "nixpkgs"; };

    snowfall-lib.inputs           = { nixpkgs.follows = "nixpkgs"; };

    # Note: flake-utils-plus.inputs       = { flake-utils.follows = "flake-utils"; };

    # Note: snowfall-lib.inputs           = { flake-utils-plus.follows = "flake-utils-plus"; };
    # Note: snowfall-lib.inputs           = { flake-compat.follows = ""; };
  };

  outputs = inputs: import ./default.nix inputs;
}
