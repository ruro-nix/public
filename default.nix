inputs:
inputs.snowfall-lib.mkFlake {
  inherit inputs;
  src = ./.;

  channels = let
    common = {
      allowUnfree = true;

      # Make sure that we are being explicit with package names/versions
      allowAliases = false;
    };
    nixpkgs-nocuda = common // { };
    nixpkgs-cuda86 = common // {
      # CUDA support
      cudaSupport = true;
      cudaCapabilities = [ "8.6" ];
    };

    # Use the cuda-capable version as the default nixpkgs
    nixpkgs = nixpkgs-cuda86;

    # Create the channels from the specified configs and the nixpkgs input
    mkChannel = name: config: {
      inherit config;
      input = inputs.nixpkgs;
    };
    channels = { inherit nixpkgs nixpkgs-nocuda nixpkgs-cuda86; };
  in
    builtins.mapAttrs mkChannel channels;

  systems.hosts = {
    nyx.channelName = "nixpkgs-cuda86";
    zeus-pyrite.channelName = "nixpkgs-nocuda";
  };

  snowfall = {
    namespace = "z";

    # TODO: FLAKE-SNOWFALL-CFG
    # meta.name = "self";
  };

  systems.modules.nixos = [ ];

  outputs-builder = channels: { formatter = channels.nixpkgs.treefmt-nixfmt; };
}
