{ config, pkgs, lib, ... }:
let
  big = lib.z.markObjectOrList { name = "big"; cond = (!config.z.device.minimal); };
  nvidia = lib.z.markObjectOrList { name = "nvidia"; cond = config.z.device.hasNvidia; };
  graphical = lib.z.markObjectOrList { name = "graphical"; cond = (!config.z.device.headless); };

  python = pkgs.python3;

  # buildEnv is similar to withPackages, but slightly more configurable
  pythonEnv = python.buildEnv.override {
    extraLibs = config.z.system.pythonLibs;
    permitUserSite = true;
    # extraOutputsToInstall = [];
  };

  # TODO: PY-PACKAGES
  pythonLibs = let
    all = with python.pkgs; {
      formatters = [
        black
        pylint
        isort
        mypy
        pyupgrade
        add-trailing-comma
      ];
      nbqa = toPythonModule ((pkgs.nbqa.override { python3 = python; }).withTools (_: all.formatters));

      cli = [
        cookiecutter
        grip
        ipython
        jupyter
        nbclassic
        pip
        pytest
        python-gitlab
        tox
        venv-nix-ld
      ];

      generic = [
        argcomplete
        baron
        docrepr
        easydict
        ffmpeg-python
        jedi
        lazy-object-proxy
        parse
        progressbar
        pybluez
        pynvim
        pyppeteer
        (big (graphical pyqt6))
        qrcode
        regex
        resolvelib
        sphinx
        tkinter
        widgetsnbextension
        xlwt
      ];

      torch-and-stuff = big [
        albumentations
        pytorch-lightning
        timm
        torch
        torchaudio
        torchvision
      ];
      scientific = [
        editdistance
        einops
        h5py
        onnx
        (big opencv4)
        opt-einsum
        (big (nvidia pycuda))
        (big scikit-image)
        (big scikit-learn)
        seaborn
        (big umap-learn)
      ];
    };
  in
    lib.z.mergeObjectOrList (lib.attrValues all);

in {
  z.system = {
    inherit pythonEnv pythonLibs;
  };
  environment.systemPackages = [ config.z.system.pythonEnv ];
}
