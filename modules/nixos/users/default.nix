{ config, pkgs, lib, ... }: {
  # Define user accounts
  users = {
    mutableUsers = false;
    users = let
      common = {
        shell = pkgs.zsh;
        password = config.z.secret.localPassword;
        # TODO: USER-NIXOS-COMMON
      };
      normal = {
        openssh.authorizedKeys.keys = config.z.secret.authorizedSSHKeys;
        isNormalUser = true;
        extraGroups =
          [ "adbusers" "audio" "bluetooth" "docker" "sudo" "wheel" ];
      };
    in {
      root = common;
      ruro = common // normal;
    };
  };
}
