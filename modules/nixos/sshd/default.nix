{ config, pkgs, lib, ... }: {
  # Enable sshd
  services.openssh = {
    enable = true;
    settings = {
      PasswordAuthentication = false;
      # TODO: CFG-SSHD
    };
  };
}
