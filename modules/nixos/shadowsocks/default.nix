{ config, pkgs, lib, ... }:
let
  commonConfig = config.z.secret.shadowsocksCreds // {
    method = "none";
    fast_open = true;
    prever_ipv6 = true;
  };
  mkShadowsocksService = { kind, configFile }:
  lib.mkIf (config.z.secret.shadowsocksCreds != null)
  {
    description = "Shadowsocks-Rust Client Service (${kind})";
    after = [ "network.target" ];
    wantedBy = [ "multi-user.target" ];
    script = ''
      exec ${pkgs.shadowsocks-rust}/bin/sslocal -c ${configFile} --log-without-time
    '';
  };

  socks5Config = commonConfig // {
    local_address = "127.0.0.1";
    local_port = 1080;
  };
  socks5ConfigFile =
    pkgs.writeText "ruro.live.json" (lib.strings.toJSON socks5Config);

  httpConfig = commonConfig // {
    protocol = "http";
    local_address = "0.0.0.0";
    local_port = 1010;
  };
  httpConfigFile = pkgs.writeText "ruro.live.json" (lib.strings.toJSON httpConfig);
in {
  systemd.services.shadowsocks-rust-socks5 = mkShadowsocksService {
    kind = "socks5";
    configFile = socks5ConfigFile;
  };

  systemd.services.shadowsocks-rust-http = mkShadowsocksService {
    kind = "http";
    configFile = httpConfigFile;
  };
}
