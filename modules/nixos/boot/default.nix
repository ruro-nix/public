{ config, pkgs, lib, ... }: {
  # Use the systemd-boot EFI boot loader.
  boot.loader = {
    systemd-boot = {
      enable = true;
      consoleMode = "auto";
      configurationLimit = 100;
    };
    efi.canTouchEfiVariables = true;
  };

  # Use the latest kernel
  # TODO: KERNEL-VER
  # Note: probably no longer needed
  # boot.kernelPackages = pkgs.linuxPackages_latest;

  boot.kernel.sysctl = {
    "kernel.panic_on_oops" = 1; # fail fast instead of waiting for a full panic
    "kernel.sysrq" = 1; # enable all SysRQs
    "vm.admin_reserve_kbytes" = 256 * 1024; # 256 MB
  };

  # kexec into a backup kernel on crash, enter systemd-rescue with dump in /proc/vmcore
  boot.crashDump.enable = true;

  # Extra kernel modules.
  boot.kernelModules = [ "coretemp" ];
}
