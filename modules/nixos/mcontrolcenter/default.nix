# TODO: UPSTREAM
{ config, pkgs, lib, ... }: let
  cfg = config.programs.mcontrolcenter;

  inherit (lib)
    mkEnableOption mkPackageOption mkIf;
in {
  options.programs.mcontrolcenter = {
    enable = mkEnableOption "MControlCenter, a graphical application for managing settings of MSI laptops";
    package = mkPackageOption pkgs "mcontrolcenter" {};

    enableEcSys = mkEnableOption "automatic loading of the ec_sys kernel module" // {
      default = true;
      extraDescription = ''
        The ec_sys kernel module is required for mcontrolcenter to communicate with the laptop.
        Alternatively, you could disable this option and manually load the acpi_ec kernel module.
      '';
    };
    enableDbusUnit = mkEnableOption "automatic loading of the system mcontrolcenter.helper dbus unit" // {
      default = true;
      extraDescription = ''
        This unit uses the mcontrolcenter-helper binary to allow normal unprivileged users to read
        and modify the laptop settings, without directly interacting with the ACPI Embedded Controller.
      '';
    };
    enableUserUnit = mkEnableOption "automatic launching of the mcontrolcenter system tray" // {
      default = false;
      extraDescription = ''
        This unit launches the main mcontrolcenter GUI/tray application
        after the graphical session has finished loading.
      '';
    };
  };

  config = mkIf cfg.enable {
    environment.systemPackages = [ cfg.package ];

    boot = mkIf cfg.enableEcSys {
      kernelModules = [ "ec_sys" ];
      extraModprobeConfig = ''
        options ec_sys write_support=1
      '';
    };

    systemd.packages = mkIf cfg.enableDbusUnit [ cfg.package ];

    systemd.user.services.mcontrolcenter = mkIf cfg.enableUserUnit {
      description = "MControlCenter GUI/tray applet";

      after = [ "graphical-session.target" ];
      bindsTo = [ "graphical-session.target" ];
      wantedBy = [ "graphical-session.target" ];

      serviceConfig.ExecStart = "${cfg.package}/bin/mcontrolcenter";
    };
  };
}
