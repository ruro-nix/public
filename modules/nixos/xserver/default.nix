{ config, pkgs, lib, ... }: {
  config = lib.mkIf (!config.z.device.headless) {
    # Enable the X11 windowing system.
    services.xserver.enable = true;

    # TODO: X-DISPLAY-MANAGER
    environment.systemPackages = [ pkgs.sddm-chili-minimal-theme ];
    services.displayManager.sddm = {
      enable = true;
      theme = "chili";
      settings.Theme.FacesDir = "${./faces}";
    };

    # TODO: X-DESKTOP-ENVIRON
    services.xserver.desktopManager.plasma5.enable = true;
    # Mask the Polkit agent service that spawns annoying GUI password prompts
    systemd.user.services.plasma-polkit-agent.enable = false;

    # services.xserver.windowManager.qtile.enable = true;

    # services.xserver.displayManager.gdm.enable = true;
    # services.xserver.displayManager.gdm.wayland = false;
    # services.xserver.desktopManager.gnome.enable = true;

    # services.xserver.desktopManager.plasma5.enable = true;
    # services.desktopManager.plasma6.enable = true;
    # services.displayManager.defaultSession = "plasmax11";

    # DPI
    # TODO: X-DPI-SCALING
    # services.xserver.dpi = 170;
    # environment.variables = {
    #   GDK_SCALE = "2";
    #   GDK_DPI_SCALE = "0.5";
    #   _JAVA_OPTIONS = "-Dsun.java2d.uiScale=2";
    # };

    # Configure keymap in X11
    services.xserver.xkb = {
      layout = "us,ru";
      options = "grp:caps_toggle,grp_led:caps";
    };

    # Enable CUPS to print documents.
    services.printing.enable = true;

    # Enable sound.
    security.rtkit.enable = true;
    services.pipewire = {
      enable = true;
      pulse.enable = true;
      alsa.enable = true;
      jack.enable = true;
    };

    # Enable bluetooth.
    hardware.bluetooth = {
      enable = true;
      # TODO: CFG-BLUETOOTH
      # settings = {
      #   General = {
      #     Enable = "Source,Sink,Media,Socket";
      #     ControllerMode = "dual";
      #     Experimental = "true";
      #     FastConnectable = "true";
      #     MultiProfile = "multiple";
      #   };
      #   Policy.AutoEnable = "true";
      # };
    };

    # Enable touchpad support.
    services.libinput = {
      enable = true;
      mouse = {
        # Use raw acceleration for the mouse
        accelProfile = "flat";
        # Don't treat lmb+rmb as middle click
        middleEmulation = false;
      };
      touchpad = {
        # Don't treat single tap as click
        tapping = false;
        # Don't allow tap-drag without holding lmb
        tappingDragLock = false;
      };
    };

    # Enable dconf (required for GTK theme support).
    services.dbus.packages = [ pkgs.dconf ];
  };
}
