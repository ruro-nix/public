{ config, pkgs, lib, ... }:
let
  neovim = pkgs.wrapNeovimUnstable pkgs.neovim-unwrapped {
    viAlias = true;
    vimAlias = true;
    python3Env = config.z.system.pythonEnv;

    # TODO: CFG-VIM

    # TODO: CFG-VIM-PLUG
    # Note: the following is a temporray hack, I should probably use
    #       pkgs.neovimUtils.makeNeovimConfig instead
    wrapRc = false;
    packpathDirs.myNeovimPackages = {
      start = [ ];
      opt = [ ];
    };
  };
in { environment.systemPackages = [ neovim ]; }
