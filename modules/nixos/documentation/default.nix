{ config, pkgs, lib, ... }: {
  # Enable developer documentation
  documentation.dev.enable = true;

  # Add standard LINUX/UNIX/POSIX/C man pages
  environment.systemPackages = with pkgs; [
    linux-manual
    man-pages
    man-pages-posix
    stdmanpages
  ];
}
