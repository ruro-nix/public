{ config, pkgs, lib, ... }: {
  # Select internationalisation properties.
  i18n.defaultLocale = "en_GB.UTF-8";
  console = {
    font = "ter-i32b";
    keyMap = "us";
    earlySetup = true;
    packages = [ pkgs.terminus_font ];
  };

  # Set your time zone.
  time.timeZone = "Europe/Moscow";
}
