e: let
  inherit (e) XDG_CONFIG_HOME XDG_DATA_HOME XDG_CACHE_HOME;
in {
  # XDG configs
  ASPELL_CONF           = "home-dir ${XDG_CONFIG_HOME}/aspell/";
  DOCKER_CONFIG         = "${XDG_CONFIG_HOME}/docker";
  GNUPGHOME             = "${XDG_CONFIG_HOME}/gnupg";
  GRIPHOME              = "${XDG_CONFIG_HOME}/grip";
  GTK2_RC_FILES         = "${XDG_CONFIG_HOME}/gtkrc-2.0";
  IPYTHONDIR            = "${XDG_CONFIG_HOME}/ipython";
  JUPYTER_CONFIG_DIR    = "${XDG_CONFIG_HOME}/jupyter";
  KDEHOME               = "${XDG_CONFIG_HOME}/kde";
  KERAS_HOME            = "${XDG_CONFIG_HOME}/keras";
  LESSKEY               = "${XDG_CONFIG_HOME}/lesskey";
  RIPGREP_CONFIG_PATH   = "${XDG_CONFIG_HOME}/ripgreprc";
  TEXMFCONFIG           = "${XDG_CONFIG_HOME}/texlive/texmf-config";
  WGETRC                = "${XDG_CONFIG_HOME}/wgetrc";
  ZDOTDIR               = "${XDG_CONFIG_HOME}/zsh";

  # XDG data
  ANDROID_AVD_HOME      = "${XDG_DATA_HOME}/android/avd";
  ANDROID_USER_HOME     = "${XDG_DATA_HOME}/android";
  CARGO_HOME            = "${XDG_DATA_HOME}/cargo";
  GRADLE_USER_HOME      = "${XDG_DATA_HOME}/gradle";
  LESSHISTFILE          = "${XDG_DATA_HOME}/lesshst";
  MACHINE_STORAGE_PATH  = "${XDG_DATA_HOME}/docker-machine";
  PYTHON_HISTORY        = "${XDG_DATA_HOME}/python_history";
  RUSTUP_HOME           = "${XDG_DATA_HOME}/rustup";
  SSB_HOME              = "${XDG_DATA_HOME}/zoom";
  TEXMFHOME             = "${XDG_DATA_HOME}/texmf";
  WINEPREFIX            = "${XDG_DATA_HOME}/wineprefix";
  _JAVA_OPTIONS         = "-Djava.util.prefs.userRoot=${XDG_DATA_HOME}/java";

  # XDG cache
  CUDA_CACHE_PATH       = "${XDG_CACHE_HOME}/cuda";
  ERRFILE               = "${XDG_CACHE_HOME}/X11/xsession-errors";
  GOPATH                = "${XDG_CACHE_HOME}/go";
  PUB_CACHE             = "${XDG_CACHE_HOME}/pub-cache";
  PYLINTHOME            = "${XDG_CACHE_HOME}/pylint";
  PYTHONPYCACHEPREFIX   = "${XDG_CACHE_HOME}/pycache";
  TEXMFVAR              = "${XDG_CACHE_HOME}/texlive/texmf-var";
  XCOMPOSECACHE         = "${XDG_CACHE_HOME}/X11/xcompose";
}
