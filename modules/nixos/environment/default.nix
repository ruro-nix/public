{ config, pkgs, lib, ... }:
let
  files = [
    # Theme colors
    ./vars/colors.nix

    # XDG Base Paths
    ./vars/xdg-homes.nix

    # Fix XDG Base non-compliant apps
    ./vars/xdg-fixes.nix
  ];

  call = file: import file vars;
  vars' = map call files;
  vars = lib.attrsets.mergeAttrsList vars';
in { environment.sessionVariables = vars; }
