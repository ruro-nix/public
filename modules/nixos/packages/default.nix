{ config, pkgs, lib, ... }:
let
  big = lib.z.markObjectOrList { name = "big"; cond = (!config.z.device.minimal); };
  nvidia = lib.z.markObjectOrList { name = "nvidia"; cond = config.z.device.hasNvidia; };
  graphical = lib.z.markObjectOrList { name = "graphical"; cond = (!config.z.device.headless); };

  # Install packages to PATH.
  pathPackages = let
    all = with pkgs; {
      themes = graphical [
        arc-theme
        arc-kde-theme
        papirus-icon-theme
      ];

      gui = graphical [
        (big audacity)
        (big discord)
        dunst # notifications
        evince # PDF viewer
        plasma5Packages.filelight
        gimp
        google-chrome
        plasma5Packages.kcolorchooser
        (big plasma5-kdenlive-with-plugins)
        (big libreoffice)
        mpv
        (big obs-with-plugins)
        (big obsidian)
        pavucontrol
        pympress # dual-screen PDF presentations with notes
        (big telegram-desktop)
        tilda
        (big winbox)
        wpa_supplicant_gui # WiFi
        (big zoom-us)
      ];

      terminal = [
        # alacritty
        alsa-utils
        bat
        bind # nslookup
        binutils # strings
        curl
        delta
        dig
        entr
        eza
        ffmpeg-full
        file
        gdb
        gitFull
        git-lfs
        gnupg
        imagemagickBig
        moreutils # errno sponge parallel
        nerdfonts-scripts # nerdfonts/i_*.sh
        numactl
        (nvidia nvitop)
        (nvidia nvtopPackages.full)
        pinentry-tty
        poppler_utils # pdfinfo pdfimages pdftotext
        pre-commit
        psmisc # killall
        pulseaudio
        pv
        ripgrep
        # shpool
        socat
        sshfs
        (big texlive.combined.scheme-full)
        traceroute
        unixtools.xxd
        unzip
        wget
        yt-dlp
        zip
      ];

      nix = [
        nix-index
        nix-output-monitor
      ];

      hardware = [
        libva-utils
        lm_sensors
        lshw
        pciutils
        (nvidia vdpauinfo)
        vulkan-tools
      ];

      x11 = graphical [
        mesa-demos # glxgears/glxinfo
        wmctrl
        xclip
        xorg.xdpyinfo
        xorg.xev
        xorg.xkill
      ];
    };
  in
    lib.z.mergeObjectOrList (lib.attrValues all);

  # Install systemd services from the following packages.
  servicePackages = with pkgs; [
    # shpool
  ];
in
{
  z.system = {
    inherit pathPackages servicePackages;
  };
  environment.systemPackages = config.z.system.pathPackages;
  systemd.packages = config.z.system.servicePackages;
}
