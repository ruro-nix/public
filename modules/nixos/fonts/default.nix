{ config, pkgs, lib, ... }: {
  fonts = {
    enableDefaultPackages = true;
    fontconfig = {
      defaultFonts = {
        emoji = [ "Noto Color Emoji" ];
        monospace = [ "Hack Nerd Font" ];
        sansSerif = [ "Noto Sans" ];
        serif = [ "Noto Serif" ];
      };
      hinting.style = "medium";
    };
    fontDir.enable = true;

    packages = with pkgs; [
      # microsoft fonts
      corefonts # Arial, Comic Sans, Courier New, Impact, Times New Roman, Webdings, Georgia, Andale, Trebuchet, Verdana
      vistafonts # Calibri, Cambria, Candara, Consolas, Constantia, Corbel
      noto-fonts
      noto-fonts-cjk-sans
      noto-fonts-color-emoji
      noto-fonts-monochrome-emoji
      fira-code
      (nerdfonts.override { fonts = [ "Hack" "Noto" "FiraCode" ]; })
    ];
  };
}
