{ config, pkgs, lib, ... }: {
  # Enable all drivers (even non-free, non-redistributable, etc)
  hardware.enableAllFirmware = true;

  # Use Sound Open Firmware drivers
  # TODO: SOUND-DRIVERS
  # Note: probably no longer needed?
  # hardware.firmware = [ pkgs.sof-firmware ];

  # Use propietary NVIDIA drivers
  services.xserver.videoDrivers = lib.mkIf config.z.device.hasNvidia [ "nvidia" ];
  hardware.nvidia = lib.mkIf config.z.device.hasNvidia {
    # Modesetting is required.
    modesetting.enable = true;

    # Enable power management (do not disable this unless you have a reason to).
    # Likely to cause problems on laptops and with screen tearing if disabled.
    powerManagement.enable = true;
    # TODO: BATTERY-FANS-POWER
    # powerManagement.finegrained = true;

    # TODO: KERNEL-PANIC
    # See: https://discourse.nixos.org/t/kernel-panic-system-freezes-no-bsod
    #      https://discourse.nixos.org/t/random-freeze-of-system
    open = false;
    package = config.boot.kernelPackages.nvidiaPackages.beta; # 560.31.02
    # package = config.boot.kernelPackages.nvidiaPackages.stable;
    # package = config.boot.kernelPackages.nvidiaPackages.mkDriver {
    #   version = "535.154.05";
    #   sha256_64bit = "sha256-fpUGXKprgt6SYRDxSCemGXLrEsIA6GOinp+0eGbqqJg=";
    #   sha256_aarch64 = "sha256-G0/GiObf/BZMkzzET8HQjdIcvCSqB1uhsinro2HLK9k=";
    #   openSha256 = "sha256-wvRdHguGLxS0mR06P5Qi++pDJBCF8pJ8hr4T8O6TJIo=";
    #   settingsSha256 = "sha256-9wqoDEWY4I7weWW05F4igj1Gj9wjHsREFMztfEmqm10=";
    #   persistencedSha256 = "sha256-d0Q3Lk80JqkS1B54Mahu2yY/WocOqFFbZVBh+ToGhaE=";
    # };

    # Use the NVidia open source kernel module (not to be confused with the
    # independent third-party "nouveau" open source driver).
    # Support is limited to the Turing and later architectures. Full list of
    # supported GPUs is at:
    # https://github.com/NVIDIA/open-gpu-kernel-modules#compatible-gpus
    # Only available from driver 515.43.04+
    # Do not disable this unless your GPU is unsupported or if you have a good reason to.
    # TODO: NVIDIA-DRIVERS
    # open = true;

    # TODO: MOD-HEADLESS
    # Note: should be set in headless mode?
    # nvidiaPersistenced = true;

    # Enable PRIME offloading
    prime = {
      # TODO: NVIDIA-DRIVERS
      offload = {
        enable = true;
        enableOffloadCmd = true;
      };
      #   OR
      # # Note: enabling modesetting together with prime.sync
      # #       causes insane flickering in GTK apps
      # sync.enable = true;
      #   OR
      # reverseSync.enable = true; (probably not)
      nvidiaBusId = "PCI:1:0:0";
      intelBusId = "PCI:0:2:0";
    };
  };

  # Enable OpenGL and hardware acceleration
  hardware.graphics = let
    common = with pkgs; [
      libGL
    ];
    intel = with pkgs; [
      intel-compute-runtime
      intel-media-driver
      vpl-gpu-rt
    ];
    nvidia = with pkgs; [
      libva-vdpau-driver
      libvdpau-va-gl
    ];
    # TODO: remove after upstream issue is resolved
    # Note: https://github.com/NixOS/nixpkgs/issues/359086
    broken-i686 = [ "vpl-gpu-rt" ];

    # Convert normal packages to corresponding 32-bit versions
    to32 = p: (
      if lib.elem "i686-linux" p.meta.platforms && !(lib.elem p.pname broken-i686)
      then [ pkgs.pkgsi686Linux.${p.pname} ]
      else []
    );
    getPackages = extraPackages: {
      inherit extraPackages;
      extraPackages32 = lib.concatMap to32 extraPackages;
    };
  in lib.mkMerge [
    {
      enable = true;
      enable32Bit = true;
    }
    (getPackages common)
    (getPackages intel) # just in case, install intel hw accel even with nvidia
    (lib.mkIf config.z.device.hasNvidia (getPackages nvidia))
  ];

  # Set the default hardware acceleration mode
  environment.variables = let
    intel = {
      LIBVA_DRIVER_NAME = "iHD";
    };
    nvidia = {
      LIBVA_DRIVER_NAME = "nvidia";
      NVD_BACKEND = "direct";
      NVD_LOG = "1";
      VDPAU_DRIVER = "va_gl";
    };
  in lib.mkMerge [
    (lib.mkIf (!config.z.device.hasNvidia) intel)
    (lib.mkIf config.z.device.hasNvidia nvidia)
  ];
}
