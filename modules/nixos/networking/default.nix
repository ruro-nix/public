{ config, pkgs, lib, ... }:
{
  # Hostname
  # TODO: MOD-PER-SYSTEM
  networking.hostId = config.z.device.hostId;

  # Configure wpa_supplicant
  networking.wireless = {
    enable = true;
    userControlled.enable = true;
    # TODO: CFG-NETWORKING-WPA-SUPPLICANT

    # Generate the WiFi network list based on
    # secret.WIFI_NETWORKS and WIFI_PASSWORDS
    networks = (lib.listToAttrs (lib.imap1 (idx:
      { ssid, key ? ssid }: {
        name = ssid;
        value = {
          priority = idx;
          psk = config.z.secret.wifiPasswords."${key}";
        };
      }) config.z.secret.wifiNetworks));
  };

  # Configure NetworkManager
  networking.networkmanager = {
    # TODO: CFG-NETWORK-MANAGER
    # enable = true;
    # dns = "none"; # controlled by dnscrypt-proxy2
  };

  # The global useDHCP flag is deprecated, therefore explicitly set to false here.
  # Per-interface useDHCP will be mandatory in the future, so this generated config
  # replicates the default behaviour.
  networking.useDHCP = false;
  networking.interfaces = let
    interfaces = config.z.device.ethrInterfaces ++ config.z.device.wlanInterfaces;
    common = { useDHCP = true; };
  in
    lib.genAttrs interfaces (name: common);

  # Use loose reverse-path filter. By default, NixOS uses a strict filter, which
  # doesn't accept incoming connections from secondary interfaces.
  # For example, connecting to the same network with Ethernet and WiFi.
  networking.firewall.checkReversePath = "loose";

  # Prevent ARP flux when multiple interfaces connect to the same subnet.
  boot.kernel.sysctl."net.ipv4.conf.all.arp_ignore" = 1;
  boot.kernel.sysctl."net.ipv4.conf.all.arp_filter" = 1;
}
