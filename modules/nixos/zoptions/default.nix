{ config, pkgs, lib, ... }:
let
  zOption = type: default: lib.mkOption {
    inherit type default;
    visible = false;
  };
  t = lib.types;
  required = throw "This option is required";
in
{
  options.z.system = {
    pythonEnv = zOption t.package required;
    pythonLibs = zOption (t.listOf t.package) required;

    pathPackages = zOption (t.listOf t.package) required;
    servicePackages = zOption (t.listOf t.package) required;
  };

  options.z.device = {
    hasNvidia = zOption t.bool required;
    headless = zOption t.bool required;
    isMSI = zOption t.bool required;
    minimal = zOption t.bool required;

    hostId = zOption t.str required;
    ethrInterfaces = zOption (t.listOf t.str) [];
    wlanInterfaces = zOption (t.listOf t.str) [];
  };

  options.z.secret = {
    authorizedSSHKeys = zOption (t.listOf t.str) [];
    bluetoothDevices = zOption (t.listOf t.str) [];
    wifiNetworks = let
      wifiNetwork = t.attrsOf t.str;
      # t.submodule {
      #   ssid = t.str;
      #   key = # optional t.str;
      # };
    in
      zOption (t.listOf wifiNetwork) [];
    wifiPasswords = zOption (t.attrsOf t.str) {};
    localPassword = zOption t.str "password";
    shadowsocksCreds = let
      credAttrs = t.attrs;
      # t.submodule {
      #   password = t.str;
      #   server = t.str;
      #   server_port = t.int;
      # };
    in
      zOption (t.nullOr credAttrs) null;
  };
}
