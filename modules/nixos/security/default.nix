{ config, pkgs, lib, ... }: {
  # Wait for sudo password forever
  # (useful for sudo nixos-rebuild after long build)
  security.sudo.extraConfig = ''
    Defaults passwd_timeout=0
  '';
}
