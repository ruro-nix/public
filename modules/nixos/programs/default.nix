{ config, pkgs, lib, ... }: {
  # Enable "magic" debugging information loading
  services.nixseparatedebuginfod.enable = true;

  # Programs with extra configuration.
  programs = {
    adb.enable = true;
    dconf.enable = true;
    iotop.enable = true;
    mcontrolcenter.enable = config.z.device.isMSI;

    htop = {
      enable = true;
      # TODO: CFG-HTOP
      # settings = { };
    };

    tmux = {
      enable = true;
      # TODO: CFG-TMUX
    };

    zsh = {
      enable = true;
      # TODO: BUG-ZSH-COMPLETIONS
      # Note: one of these options breaks TAB completion
      # enableCompletion = true;
      # enableBashCompletion = true;
      # syntaxHighlighting = { enable = true; };
      # autosuggestions = { enable = true; };

      # I do compinit in zshrc (via antigen) to customize .zcompdump location
      enableGlobalCompInit = false;

      # TODO: CFG-ZSH
    };

    nix-ld = {
      enable = true;
      libraries = with pkgs; lib.mkMerge [
        (lib.mkIf config.z.device.hasNvidia [
          config.hardware.nvidia.package
          cudatoolkit
        ])
        [
          # TODO: CFG-NIX-LD
          # Also, fix python venvs (see the https://github.com/Mic92/nix-ld README)

          glib
          stdenv.cc.cc.lib
          xorg.libX11
          xorg.libXrender
          zlib
        ]
      ];
    };

    gnupg.agent = {
      # TODO: CFG-GPG
      # enable = true;
      # enableSSHSupport = true;
      # pinentryFlavor = "tty";
    };

    chromium = {
      # TODO: CFG-CHROME-OPTS
      # Note: https://chromeenterprise.google/policies/
      # Note: chrome-flags.conf (?)
      # extraOpts = {};

      # TODO: CFG-CHROME-EXT
      extensions = [
        # Note: I think these don't actually work with Google Chrome?
        "cfhdojbkjhnklbpkdaibdccddilifddb" # Adblock Plus - free ad blocker
        "gkeojjjcdcopjkbelgbcpckplegclfeg" # AdGuard Extra
        "ghbmnnjooekpmoecnnnilnnbdlolhkhi" # Google Docs Offline
        "oldceeleldhonbafppcapldpdifcinji" # Grammar Checker & Paraphraser - Language Tool
        "bcjindcccaagfpapjjmafapmmgkkhgoa" # JSON Formatter
        "pcmpcfapbekmbjjkdalcgopdkipoggdi" # MarkDownload - Markdown Web Clipper
        # "dndlcbaomdoggooaficldplkcmkfpgff" # New Tab, New Window
        "jmphljmgnagblkombahigniilhnbadca" # Open link in same tab, pop-up as tab
        "cimiefiiaegbelhefglklhhakcgmhkai" # Plasma Integration
        "edacconmaakjimmfgnblocblbcdcpbko" # Session Buddy
        "cmhmepchidgajcngdkpdchbdboheihgi" # Shorts to Normal player
        # "hbhkfnpodhdcaophahpkiflechaoddoi" # Styler Beta
        "cgjalgdhmbpaacnnejmodfinclbdgaci" # Syntaxtic!
        # "dhdgffkkebhmkfjojejmpbldmpobfkfo" # Tapermonkey
        "bbppejejjfancffmhncgkhjdaikdgagc" # Undisposition
        # "nffaoalbilbmmfgbnbgppjihopabppdk" # Video Speed Controller
      ];
    };
  };
}
