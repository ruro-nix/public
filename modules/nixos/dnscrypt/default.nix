{ config, pkgs, lib, ... }:
let StateDirectory = "dnscrypt-proxy";
in {
  # Disable DNS from DHCP
  networking = {
    nameservers = [ "127.0.0.1" "::1" ];
    dhcpcd.extraConfig = "nohook resolv.conf";
  };

  # Configure dnscrypt-proxy2
  services.dnscrypt-proxy2 = {
    enable = true;
    settings = {
      ipv6_servers = true;
      require_dnssec = true;
      require_nolog = true;
      require_nofilter = true;

      sources.public-resolvers = {
        urls = [
          "https://raw.githubusercontent.com/DNSCrypt/dnscrypt-resolvers/master/v3/public-resolvers.md"
          "https://download.dnscrypt.info/resolvers-list/v3/public-resolvers.md"
        ];
        cache_file = "/var/lib/${StateDirectory}/public-resolvers.md";
        minisign_key =
          "RWQf6LRCGA9i53mlYecO4IzT51TGPpvWucNSCh1CBM0QTaLn73Y7GFO3";
      };

      # TODO: DNS-LOCAL-FWD
      # Note: *.tevian.ru *.graphicon.ru
      # Note: forwarding_rules = "path/to/rules.txt";
      # Note: https://github.com/DNSCrypt/dnscrypt-proxy/blob/master/dnscrypt-proxy/example-forwarding-rules.txt
    };
  };

  # Make sure that the StateDirectory didn't change
  systemd.services.dnscrypt-proxy2.serviceConfig = { inherit StateDirectory; };
}
