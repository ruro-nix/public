{ config, pkgs, lib, ... }:
let
  substituters = import ./substituters.nix;
  urls = map (x: x.url) substituters;
  keys = map (x: x.key) substituters;
in {
  # Use the "default" nix package (it is set in overlays).
  nix.package = pkgs.nix;

  # Configure nix
  nix.settings = {
    experimental-features = [ "nix-command" "flakes" ];
    flake-registry = "";
    keep-failed = true;
    keep-going = true;
    substituters = urls;
    trusted-substituters = urls;
    trusted-public-keys = keys;
    trusted-users = [ "@wheel" ];
    use-xdg-base-directories = true;
  };

  # Propagate inputs to registry and NIX_PATH
  nix.linkInputs = true;
  nix.generateNixPathFromInputs = true;
  nix.generateRegistryFromInputs = true;

  # Add fake next-self input (/etc/nixos)
  nix.registry.next-self.to = {
    type = "path";
    path = "/etc/nixos";
  };
  environment.etc."nix/inputs/next-self".source = "/etc/nixos";

  # OOM configuration:
  systemd = {
    # Kill the local nix builds if they use too much memory
    services."nix-daemon".serviceConfig.MemoryMax = "50%";

    # If a kernel-level OOM event does occur anyway,
    # strongly prefer killing nix-daemon child processes
    services."nix-daemon".serviceConfig.OOMScoreAdjust = 1000;
  };
}
