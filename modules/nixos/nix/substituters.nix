[
  { url = "https://cuda-maintainers.cachix.org";  key = "cuda-maintainers.cachix.org-1:0dq3bujKpuEPMCX6U4WylrUDZ9JyUG0VpVZa7CNfq5E="; }
  { url = "https://nix-community.cachix.org";     key = "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="; }
  { url = "https://numtide.cachix.org";           key = "numtide.cachix.org-1:2ps1kLBUWjxIneOy1Ik6cQjb41X0iXVXeHigGmycPPE="; }

# # Already included via the default cache.nixos.org
# { url = "https://hydra.nixos.org";              key = "hydra.nixos.org-1:CNHJZBh9K4tP3EKF6FkkgeVYsS3ohTl+oS0Qa8bezVs="; }

# # Currently not used revisit if/when I set up multi-python
# { url = "https://nixpkgs-python.cachix.org";    key = "nixpkgs-python.cachix.org-1:hxjI7pFxTyuTHn2NkvWCrAUcNZLNS3ZAvfYNuYifcEU="; }
]
