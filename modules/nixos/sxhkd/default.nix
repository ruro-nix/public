{ config, pkgs, lib, ... }: {
  systemd.user.services.sxhkd = {
    description = "Simple X Hotkey Daemon";
    documentation = [ "man:sxhkd(1)" ];

    after = [ "graphical-session.target" ];
    bindsTo = [ "graphical-session.target" ];
    wantedBy = [ "graphical-session.target" ];

    serviceConfig = {
      ExecStart = "${pkgs.sxhkd}/bin/sxhkd";
      ExecReload = "${pkgs.util-linux}/bin/kill -SIGUSR1 $MAINPID";
    };
  };
}
