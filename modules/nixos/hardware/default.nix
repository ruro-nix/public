{ config, pkgs, lib, ... }: {
  # TODO: MOD-SPLIT
  # Note: Extract everything from this file to appropriate places
  boot.initrd.availableKernelModules = [
    "ahci"
    "nvme"
    "rtsx_pci_sdmmc"
    "sd_mod"
    "thunderbolt"
    "usb_storage"
    "usbhid"
    "xhci_pci"
  ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-intel" ];
  boot.blacklistedKernelModules = [ "uvcvideo" ]; # disable webcam by default
  boot.extraModulePackages = [ ];

  fileSystems."/" = {
    device = "rpool/empty";
    fsType = "zfs";
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-label/BOOT";
    fsType = "vfat";
  };

  swapDevices = [ ];

  environment.etc."sysconfig/lm_sensors".source = ./lm_sensors;

  powerManagement.cpuFreqGovernor = lib.mkDefault "powersave";
  hardware.cpu.intel.updateMicrocode =
    lib.mkDefault config.hardware.enableRedistributableFirmware;
}
