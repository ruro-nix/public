{ config, pkgs, lib, ... }: {
  virtualisation.docker.enable = true;
  hardware.nvidia-container-toolkit.enable = config.z.device.hasNvidia;
}
