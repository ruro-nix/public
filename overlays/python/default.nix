{ channels, ... }:
final: prev:
let
  packageExtensions = finalPython: prevPython: {
    # Required for pretty IPython help formatting
    docrepr = finalPython.buildPythonPackage rec {
      pname = "docrepr";
      version = "0.2.0";
      # format = "setuptools";

      src = final.fetchPypi {
        inherit pname version;
        hash = "sha256-OtYuzaoLerkNzZwpq8mMrn1h0JM58YOW1MpbirQci5A=";
      };
      buildInputs = with finalPython; [ docutils jinja2 sphinx ];

      nativeCheckInputs = with finalPython; [ ipython matplotlib numpy pytest ];
      pythonImportsCheck = [ "docrepr.sphinxify" ];

      meta = with final.lib; {
        changelog =
          "https://github.com/spyder-ide/docrepr/releases/tag/${version}";
        homepage = "https://github.com/spyder-ide/docrepr";
        description = "Renders Python docstrings to rich HTML";
        license = licenses.bsd3;
        # maintainers = with maintainers; [ ruro ];
      };
    };

    venv-nix-ld = finalPython.buildPythonPackage rec {
      pname = "venv-nix-ld";
      version = "0.0.1";

      src = ./venv-nix-ld;
      pythonImportsCheck = [ "venv_nix_ld" ];

      meta = with final.lib; {
        description = "Monkeypatch venv to add LD_LIBRARY_PATH to activation scripts";
      };
    };

    # TODO: after next nixpkgs bump - remove overridePythonAttrs, just override to ffmpeg_7-full
    # ffmpeg-python = prevPython.ffmpeg-python.override { ffmpeg_7 = final.ffmpeg_7-full; };
    #   .overridePythonAttrs (prevAttrs: {
    #     patches = prevAttrs.patches ++ [
    #       # Fix ffmpeg/tests/test_ffmpeg.py: test_pipe() (v1: ignore duplicate frames)
    #       # https://github.com/kkroening/ffmpeg-python/pull/726
    #       (final.fetchpatch2 {
    #         url = "https://github.com/kkroening/ffmpeg-python/commit/557ed8e81ff48c5931c9249ec4aae525347ecf85.patch?full_index=1";
    #         hash = "sha256-XrL9yLaBg1tu63OYZauEb/4Ghp2zHtiF6vB+1YYbv1Y=";
    #       })

    #       # Fix `test__probe` on FFmpeg 7
    #       # https://github.com/kkroening/ffmpeg-python/pull/848
    #       (final.fetchpatch2 {
    #         url = "https://github.com/kkroening/ffmpeg-python/commit/eeaa83398ba1d4e5b470196f7d4c7ca4ba9e8ddf.patch?full_index=1";
    #         hash = "sha256-/qxez4RF/RPRr9nA+wp+XB49L3VNgnMwMQhFD2NwijU=";
    #       })
    #     ];
    #   });

    # TODO: MKL
    # Note: https://github.com/NixOS/nixpkgs/issues/269271
    # torch = prevPython.torch.overridePythonAttrs (prevAttrs: {
    #   patches = (prevAttrs.patches or []) ++ [ ./remove-mkl-root-hack.diff ];
    # });

    # Fix numpy+scipy blas shenanigans
    # TODO: PY-FINAL-PREV-ATTRS
    # Note: https://github.com/NixOS/nixpkgs/issues/258246
    # TODO: MKL
    # Note: https://github.com/NixOS/nixpkgs/issues/269271
    # numpy = prevPython.numpy.overridePythonAttrs (prevAttrs: {
    #   passthru = prevAttrs.passthru // { blas = final.blas; };
    #   patches = prevAttrs.patches
    #     ++ [ ./disable-failing-mkl-test-overrides.patch ];
    # });

    # scipy = prevPython.scipy.overridePythonAttrs (prevAttrs: {
    #   postPatch = (prevAttrs.postPatch or "") + ''
    #     sed 's/openblas/${
    #       final.lib.getName finalPython.scipy.passthru.blas
    #     }/g' -i meson.build
    #   '';
    # });
  };
in {
  pythonPackagesExtensions = prev.pythonPackagesExtensions
    ++ [ packageExtensions ];
}
