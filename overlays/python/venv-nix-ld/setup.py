# pylint: skip-file

import logging
import os.path

from setuptools import setup
from setuptools.command.build_py import build_py


class build_py(build_py):
    def run(self):
        super().run()

        pkg_name = self.distribution.metadata.name
        (source_root,) = self.package_dir.values()
        site_packages = self.build_lib

        src = os.path.join(source_root, f"{pkg_name}.pth")
        dst = os.path.join(site_packages, f"aaaaa_{pkg_name}.pth")

        self.announce(f"Installing .pth file {src!r} to {dst!r}", logging.WARNING)
        self.copy_file(src, dst, preserve_mode=0)


setup(cmdclass={"build_py": build_py})
