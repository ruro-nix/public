DEACTIVATE_HEADER = r"""
    # reset old environment variables
"""
DEACTIVATE_EXTENSION = r"""
    if [ -n "${_OLD_LD_LIBRARY_PATH:-}" ] ; then
        LD_LIBRARY_PATH="${_OLD_LD_LIBRARY_PATH:-}"
        export LD_LIBRARY_PATH
        unset _OLD_LD_LIBRARY_PATH
    fi
"""
ACTIVATE_HEADER = r"""
deactivate nondestructive
"""
ACTIVATE_EXTENSION = r"""
_OLD_LD_LIBRARY_PATH="$LD_LIBRARY_PATH"
LD_LIBRARY_PATH="/run/opengl-driver/lib:/run/current-system/sw/share/nix-ld/lib:$LD_LIBRARY_PATH"
export LD_LIBRARY_PATH
"""


def monkey_patch():
    """Apply patch to the venv module"""
    import sys

    # Ensure that the monkey patch is applied at most once.
    sys.modules[__package__].monkey_patch = lambda: None

    # Actually apply the patch
    import venv

    def post_setup(self, context):
        import os

        act = os.path.join(context.bin_path, "activate")
        with open(act, "r") as f:
            text = f.read()

        for h, e in [
            (DEACTIVATE_HEADER, DEACTIVATE_EXTENSION),
            (ACTIVATE_HEADER, ACTIVATE_EXTENSION),
        ]:
            text = text.replace(h, h + e)

        with open(act, "w") as f:
            f.write(text)

        return original_post_setup(self, context)

    original_post_setup = venv.EnvBuilder.post_setup
    venv.EnvBuilder.post_setup = post_setup
