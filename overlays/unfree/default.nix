{ lib, ... }:
final: prev:
let
  currentSystem = final.stdenv.hostPlatform.system;
  isSupportedBy = target: lib.elem currentSystem target.meta.platforms;
  overrideIfSupported = pkg: target: func:
    if (isSupportedBy target) then (pkg.override func) else pkg;

  withMklProvider = name:
    overrideIfSupported prev.${name} final.mkl {
      "${name}Provider" = final.mkl;
    };
in {
  # TODO: MKL
  # Note: https://github.com/NixOS/nixpkgs/issues/269271
  # blas = withMklProvider "blas";
  # lapack = withMklProvider "lapack";

  # TODO: CUDA
  # cudaPackages_12_1 = prev.cudaPackages_12_1.overrideScope
  #   (finalCuda: prevCuda: {
  #     cutensor = (prevCuda.cutensor.override {
  #       version = "1.7.0.1";
  #       hash = "sha256-3TVXiRNxoZ5z58lV7+U4OwvulUq6ajDkiSsOesud6yY=";
  #     }).overrideAttrs (final: prev: {
  #       nativeBuildInputs = prev.nativeBuildInputs
  #         ++ [ finalCuda.autoAddOpenGLRunpathHook ];
  #     });
  #   });
  # cudaPackages = final.cudaPackages_12_1;
}
