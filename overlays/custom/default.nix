{ ... }:
final: prev: {
  # TODO: UPSTREAM
  # TODO: Add the cheatsheet script
  # TODO: Improve https://github.com/ryanoasis/nerd-fonts/pull/1694
  nerdfonts-scripts = final.stdenv.mkDerivation (finalAttrs: {
    name = "nerdfonts-scripts";
    version = final.nerdfonts.version;

    src = final.fetchFromGitHub {
      owner = "ryanoasis";
      repo = "nerd-fonts";
      rev = "v${final.nerdfonts.version}";
      sparseCheckout = [ "bin/scripts" ];
      hash = "sha256-JfuGSic0sU7gMQ9tqow6/65PxGOSNGgMXMG+UPhhqFQ=";
    };

    installPhase = ''
      mkdir -p "$out/bin/nerdfonts"
      cp bin/scripts/lib/i_*.sh "$out/bin/nerdfonts/"
    '';
  });

  treefmt-nixfmt = final.writeShellApplication {
    name = "treefmt-nixfmt";
    runtimeInputs = [ final.treefmt2 final.nixfmt-classic ];
    text = ''treefmt "$@"'';
  };

  # TODO: UPSTREAM
  mcontrolcenter = final.libsForQt5.callPackage
    ({ stdenv, lib, fetchFromGitHub, qtbase, qttools, wrapQtAppsHook, cmake }:
      stdenv.mkDerivation (finalAttrs: {
        name = "mcontrolcenter";
        version = "0.4.1";

        src = fetchFromGitHub {
          owner = "dmitry-s93";
          repo = "MControlCenter";
          rev = finalAttrs.version;
          hash = "sha256-SV78OVRGzy2zFLT3xqeUtbjlh81Z97PVao18P3h/8dI=";
        };

        buildInputs = [ qtbase qttools ];
        nativeBuildInputs = [ cmake wrapQtAppsHook ];

        postPatch = ''
          patchShebangs \
              --build \
              scripts/*.sh

          sed \
              --in-place \
              "s|/usr/|$out/|g" \
              scripts/install.sh \
              src/helper/mcontrolcenter.helper.service

          sed \
              --in-place \
              "s|^Exec=mcontrolcenter$|Exec=$out/bin/mcontrolcenter|" \
              resources/mcontrolcenter.desktop
        '';

        postInstall = ''
          cd ../scripts
          ./create_installer.sh
          tar -xzvf MControlCenter-${finalAttrs.version}.tar.gz
          cd MControlCenter-${finalAttrs.version}
          ./install.sh
        '';

        meta = with lib; {
          description = "An application that allows you to change the settings of MSI laptops running Linux";
          homepage = "https://github.com/dmitry-s93/MControlCenter";
          license = with licenses; [ gpl3Plus ];
          # maintainers = with maintainers; [ ruro ];
          platforms = with platforms; linux;
        };
      })) { };
}
