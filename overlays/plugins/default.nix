{ ... }:
final: prev: {
  obs-with-plugins = final.wrapOBS {
    plugins = [
      # final.obs-studio-plugins.wlrobs # screen recording on wlroots-based wayland compositors
      final.obs-studio-plugins.obs-pipewire-audio-capture
    ];
  };

  plasma5-kdenlive-with-plugins = final.plasma5Packages.kdenlive.overrideAttrs
    (finalAttrs: prevAttrs: {
      nativeBuildInputs = (prevAttrs.nativeBuildInputs or [ ])
        ++ [ final.makeBinaryWrapper ];
      postInstall = (prevAttrs.postInstall or "") + ''
        wrapProgram $out/bin/kdenlive --prefix LADSPA_PATH : ${final.rnnoise-plugin}/lib/ladspa
      '';
      # TODO: KDENLIVE-PLUGINS
      # - For Speech Recognition
      #   - python3.srt
      #   - python3.vosk
      #   - python3.openai-whisper
    });
}
