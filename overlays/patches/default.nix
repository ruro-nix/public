{ channels, ... }:
final: prev: {
  sddm-chili-minimal-theme = let
    original = final.sddm-chili-theme;
    no-background = { themeConfig.blur = false; };
    no-capitalization = finalAttrs: prevAttrs: {
      preInstall = prevAttrs.preInstall + ''

        sed 's|\(font\.capitalization: Font\.Capitalize\)|// \1|g' \
          -i components/UserDelegate.qml

        cp ${./background.jpg} assets/background.jpg
      '';
    };
  in (original.override no-background).overrideAttrs no-capitalization;

  # delta = prev.delta.overrideAttrs (finalAttrs: prevAttrs: {
  #   version = "git";
  #   cargoDeps = prevAttrs.cargoDeps.overrideAttrs (_: {
  #     inherit (finalAttrs) src;
  #     name = "${finalAttrs.pname}-vendor.tar.gz";
  #     outputHash = "sha256-CsnXvBnYdO3FjeRKCDxxJlfSDbGto60/FJ6Icu3guwY=";
  #   });
  #   src = final.fetchFromGitHub {
  #     inherit (prevAttrs.src) owner repo;
  #     rev = "dcae5bcc2428d1fb6f5ff7b9cddd7f268d9a3735";
  #     hash = "sha256-n56ISRQbpJp1Myi6wp+F5LES2V34NvymBfc5K5glXtI=";
  #   };
  #   checkFlags = prevAttrs.checkFlags
  #     ++ [ "--skip=test_env_parsing_with_pager_set_to_bat" ];
  # });

  # tilda = final.enableDebugging (prev.tilda.overrideAttrs
  #   (finalAttrs: prevAttrs: {
  #     separateDebugInfo = true;
  #
  #     version = "2.0.0-next";
  #     src = final.fetchFromGitHub {
  #       inherit (prevAttrs.src) owner repo;
  #       rev = "ba6a69492a141b5d4bb7b513b0ed5fffc572cd27";
  #       hash = "sha256-FG/D+6+YLo3sRVcOebDOne03c280uOW+F55HTfedCBc=";
  #     };
  #
  #     patches = let
  #       delta = { a, b }:
  #         "https://github.com/lanoxx/tilda/compare/${a}..${b}.patch";
  #     in (prevAttrs.patches or [ ]) ++ [
  #       (final.fetchpatch {
  #         name = "tilda-pull-506-add-bold-bright.patch";
  #         url = delta {
  #           a = "51a980a55ad6d750daa21d43a66d44577dad277b";
  #           b = "ee78a5eea8232f955f774fd8831255168aab3ca2";
  #         };
  #         hash = "sha256-FXIJXIGThYPMF2QomNTuWmfZ+XU+6lWyfD+syQbSqFU=";
  #       })
  #       (final.fetchpatch {
  #         name = "tilda-pull-507-insert-tab-after-current.patch";
  #         url = delta {
  #           a = "51a980a55ad6d750daa21d43a66d44577dad277b";
  #           b = "ad123d9c22891c4d7a28763eaa342c4ab6772423";
  #         };
  #         hash = "sha256-YwmvLSY3M15WZ7YEKfjr/715TYGEBLicRNrqTp1kHfo=";
  #       })
  #     ];
  #   }));
}
