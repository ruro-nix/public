{ channels, ... }:
final: prev: {
  # Use the latest version of nix system-wide
  nix = final.nixVersions.latest;

  # Use the full version of ffmpeg system-wide
  # ffmpeg = prev.ffmpeg-full;

  # Use the python-enabled opencv as the system-wide opencv
  opencv4 = prev.opencv4.override { enablePython = true; };
}
